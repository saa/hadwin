# hadwinPPSAPI

# Interface to the Proofpoint Protection Server Public API

import sys
import logging
import logging.config
import requests
import hadwinImap
import time
from enum import Enum, auto

logger = logging.getLogger( sys.argv[0] + ".hadwinPPSAPI" )

class PPSQSearchState(Enum):
		""" 
			Different states that can occur after searching the quarantine.
		"""
		MOVED = auto()				# successfully moved the message to intended folder
		NOT_FOUND = auto()			# could not find the message in configured qfolders
		EXCEPTION = auto()			# an exception occurred
		LOWPRIO_NOT_SPAM = auto()	# in false-negative mode, reported message was tagged low-priority
		NOT_ENOUGH_KEYS = auto()    # could not find enough keys in message to search for

class PPSAPI:
	
	def __init__(self, ppsconf):
		logger.debug(f"hadwinPPSAPI class init for uri={ppsconf['uri']}")
		passf = open(ppsconf['passwordfile'])
		self.password = passf.read()
		if (self.password[-1] == "\n"):
			self.password = self.password[:-1]  # Got rid of the newline
		self.ppsconf = ppsconf

	def getSess(self):
		if hasattr(self, '_session'):
			return self._session
		else:
			s = requests.Session()
			s.auth = (self.ppsconf['user'], self.password)
			s.verify = "/etc/pki/tls/certs/uvm-ca-bundle.crt"
			s.headers.update({'user-agent': 'hadwin/0.1', 'accept': 'application/json'})
			self._session = s
			return s

	def moveMsgToQFolder(self, localguid, srcfoldername, destfoldername):
		logger.debug(f"moving {localguid} from {srcfoldername} to {destfoldername}")
		s = self.getSess()
		payload = { 'action' : 'move', 'localguid' : localguid, 'folder' : srcfoldername, 'targetfolder' : destfoldername }
		r = s.post(self.ppsconf['uri'] + 'rest/v1/quarantine', json=payload )
		if (r.status_code != 200):
			logger.error(f"Got status code {r.status_code}, text is {r.text}")
			logger.error(f"Request text was {r.request.body}")
			raise Exception(f"Unexpected status code {r.status_code}")
		j = r.json()
		if j:
			logger.debug(f"moveMsgToQFolder: r.json is {j}")
		else:
			raise Exception(f"Could not parse JSON in response: {r.code} {r.text}")
		

	def ctimeToIsowindow(ctimestr, windowSecs):
		'''
			Take in a ctimestr, like: Thu, 29 Aug 2019 16:42:52 -0400
			return 2 other ctimestrs, creating an "early" and "late" time which make a windowSecs window
			that is centered around ctimestr.

			This is used for generating startdate/enddate strings for PPS quarantine search
		'''

		#Thu, 29 Aug 2019 16:42:51 -0400

		parseTimeFormat = "%a, %d %b %Y %H:%M:%S %z"
		timeFormat= "%Y-%m-%d %H:%M:%S"
		centerTimestruct = time.strptime(ctimestr,parseTimeFormat)
		centerTime = time.mktime(centerTimestruct)
		earlyTime = centerTime - (windowSecs / 2)
		lateTime = centerTime + (windowSecs / 2)
		earlyTimestruct = time.gmtime(earlyTime)
		lateTimestruct = time.gmtime(lateTime)
		earlyTimestr = time.strftime(timeFormat,earlyTimestruct)
		lateTimestr = time.strftime(timeFormat,lateTimestruct)
		return((earlyTimestr, lateTimestr))

	def searchQFolder(self, payload):
		foundlocalguid = None
		foundfolder = None
		s = self.getSess()
		r = s.get(self.ppsconf['uri'] + 'rest/v1/quarantine', params=payload )
		logger.debug(f"r is {r}")
		if (r.status_code != 200):
			logger.error(f"Unexpected status code from quarantine search: {r.status_code}  Response: {r.text}")
			raise Exception(f"Unexpected status code from quarantine search: {r.status_code} Response: {r.text}")
		j = r.json()
		if j:
			logger.debug(f"r.json is: {j}")
		else:
			raise Exception(f"could not parse json.  r.text is: {r}")
		logger.debug(f"count is {j['count']}")
		if j['count'] > 0:
			# WTH is a "localguid"?  Me thinks someone at Proofpoint doesn't know what GUID means.
			foundlocalguid = j['records'][0]['localguid']
			foundfolder = j['records'][0]['folder']
		return(foundfolder, foundlocalguid)

	def search(self, imap, skd):
		logger.debug(f"Doing a PPS search for: {skd}")
		s = self.getSess()
		(early_time, late_time) = PPSAPI.ctimeToIsowindow(skd['recvdatestr'], 900)
		logger.debug(f"early time: {early_time}, late time: {late_time}")
		payload = { 'rcpt' : skd['rcptto'], 'subject' : skd['subject'], 'startdate' : early_time, 'enddate' : late_time  }
		foundlocalguid = False
		foundfolder = False
		for qfoldername in imap.imapCfg['searchqfolders']:
			payload['folder'] = qfoldername
			(foundfolder, foundlocalguid) = self.searchQFolder(payload)
			if foundlocalguid: 
				logger.debug(f"foundlocalguid is {foundlocalguid}")
				self.moveMsgToQFolder(foundlocalguid, foundfolder, imap.imapCfg['moveqfolder'])
				return PPSQSearchState.MOVED
		if (imap.imapCfg['function'] == 'false-negative'):
			# Try to find it in the bulk / low-priority folder.
			payload['folder'] = imap.imapCfg['lpqfolder']
			(foundfolder, foundlocalguid) = self.searchQFolder(payload)
			if foundlocalguid:
				logger.debug("low priority message, not spam.")
				return PPSQSearchState.LOWPRIO_NOT_SPAM
			else:
				logger.debug("cound not find message.")
				return PPSQSearchState.NOT_FOUND
		else:
			logger.debug("did not find any message. darn it.")
			return PPSQSearchState.NOT_FOUND

# vim: ts=4:sw=4
