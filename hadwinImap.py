# does IMAP stuff for hadwin

import sys
import traceback
import logging
import logging.config
import imapclient
import email
import email.parser
import email.policy
import re
import hadwinPPSAPI
from hadwinPPSAPI import PPSQSearchState


logger = logging.getLogger( sys.argv[0] + ".hadwinImap" )

class hadwinImap:

	def recursiveDescentDict(d):
		for (key, value) in d.items():
			if not isinstance(value, dict):
				logger.debug(f" key {key}:  value {value}")
			else:
				logger.debug(f" key {key} is a dict...")
				hadwinImap.recursiveDescentDict(value)

	def createFolders(self):
		"""
			Create any IMAP folders that hadwin relies on and do not yet exist.
		"""
		c = self.getClient()
		for f in ('not-found-ifolder', 'lp-not-spam-ifolder', 'done-ifolder', 'exception-ifolder',
				'not-identifiable-ifolder', 'disallowed-senders-ifolder'):
			if (f in self.imapCfg) :
				foldername = self.imapCfg[f]
				if (not c.folder_exists(foldername)):
					c.create_folder(foldername)
	
	def __init__(self, imapCfg):
		logger.debug(f"hadwinImap class init for server={imapCfg['server']}, username {imapCfg['username']} ")
		passf = open(imapCfg['passwordfile'])
		self.password = passf.read()
		if (self.password[-1] == "\n"):
			self.password = self.password[:-1]  # Got rid of the newline
		self.imapCfg = imapCfg
		#logger.debug(f"password  is {self.password}(end of string)")
		self.createFolders()

	def setPPSAPI(self, ppsapi: hadwinPPSAPI.PPSAPI):
		self._ppsapi = ppsapi

	def getClient(self):
		if not hasattr(self, '_imapclient'):
			self._imapclient = imapclient.IMAPClient(host=self.imapCfg['server']) 
			self._imapclient.login(self.imapCfg['username'], self.password)
		return(self._imapclient)

	def handleForward(self, att: email.message.EmailMessage):
		logger.debug("handleforward: parts of attachment....")
		pl = att.get_payload()
		logger.debug(f"pl is {pl}")
		assert(pl.__len__() == 1)

		pp_received_re = re.compile( r"from (?P<heloname>\w+\.in-mail\.uvm\.edu).*by (?P<ppservername>pp\-vm\d\.uvm\.edu) with E?SMTP id (?P<esmtpid>[\w\-]+)\s*\([^)]*\)\s*for <(?P<rcptto>[\w@\.\-]+)>\;\s+(?P<recvdatestr>.*)", flags=re.DOTALL|re.IGNORECASE)
		skd = { }  # quarantine search key dictionary
		#logger.debug(f"pp_received_re is a {pp_received_re}")
		for (header, value) in pl[0].items():
			#logger.debug(f"pl0 header: {header} : {value}")
			if (header == "Received"):
				#logger.debug("Received header, does it match...")
				m = pp_received_re.search(value)
				if m:
					logger.debug(f"Match, from is {m.group(1)}, ppservername is {m.group(2)}, esmtpid is {m.group(3)}, rcptto is {m.group(4)}, recvdate is {m.group(5)}")
					for k in ('ppservername', 'esmtpid', 'rcptto', 'recvdatestr'):
						skd[k] = m.group(k)
			elif (header == "Subject"):
				logger.debug("Got subject")
				skd['subject'] = value
		if (len(skd) > 1):  
			# we'd like to search using these keys
			return self._ppsapi.search(self, skd)
		else:
			return hadwinPPSAPI.PPSQSearchState.NOT_ENOUGH_KEYS

	def imapMove(self, messageid, folder):
		client = self.getClient()
		client.move(messages = [messageid], folder=folder)
	
	def fetchAndProcess(self, messageid):
		client = self.getClient()
		m = client.fetch([messageid], b'RFC822')
		#logger.debug(f"messageid {messageid}: message contains...")
		#hadwinImap.recursiveDescentDict(m)
		#for k in m:
		#	logger.debug(f"key {k}")
		e = email.parser.BytesParser(policy=email.policy.default).parsebytes(m[messageid][b'RFC822'])
		#logger.debug(f"parsed an email {e}")
		if (e.is_multipart()):
			logger.debug("message is multipart.")
		else :
			logger.debug("message is NOT multipart, moving to not-identifiable-ifolder.")
			self.imapMove(messageid, self.imapCfg['not-identifiable-ifolder'])
			return(False)
		state = None
		try:
			for att in e.iter_attachments():
				ct = att.get_content_type()
				logger.debug(f"message attachment: {ct}")
				if (ct == "message/rfc822"):
					state = self.handleForward(att)
					logger.debug(f"fetchAndProcess: state is {state}")
					if (state == PPSQSearchState.MOVED):
						self.imapMove(messageid, self.imapCfg['done-ifolder'])
					elif (state == PPSQSearchState.NOT_FOUND):
						self.imapMove(messageid, self.imapCfg['not-found-ifolder'])
					elif (state == PPSQSearchState.NOT_ENOUGH_KEYS):
						self.imapMove(messageid, self.imapCfg['not-identifiable-ifolder'])
					elif (state == PPSQSearchState.EXCEPTION):
						self.imapMove(messageid, self.imapCfg['exception-ifolder'])
					elif (state == PPSQSearchState.LOWPRIO_NOT_SPAM):
						self.imapMove(messageid, self.imapCfg['lp-not-spam-ifolder'])
					else:
						raise Exception(f"Unexpected state return from handleForward: {state}")
			if (state == None):
				# We never found any proper rfc822 attachments?
				self.imapMove(messageid, self.imapCfg['not-identifiable-ifolder'])
		except:
			ei = sys.exc_info()
			logger.exception(f"Caught an exception in fetchAndProcess for messageid {messageid}:")
			self.imapMove(messageid, self.imapCfg['exception-ifolder'])
			
	def processOnce(self):	
		logger.debug(f"processing one time for {self.imapCfg['username']}@{self.imapCfg['server']}")
		client = self.getClient()
		client.select_folder("INBOX")
		messages = client.search([b'UNSEEN', b'NOT', b'DELETED'])
		response = client.fetch(messages, ['FLAGS', 'RFC822.SIZE'])
		for messageid, data in response.items():
			logger.debug(f"messageid {messageid}: {data[b'RFC822.SIZE']} bytes")
			# let's fetch it and see what we get.
			self.fetchAndProcess(messageid)
			client.set_flags([messageid], b'UNSEEN' )
				

		
# vim: ts=4:sw=4
