#!/usr/bin/env python

# hadwin - CLI frontend for hadwin

# Connects to IMAP mailbox(es), e.g. is-spam@example.com, not-spam@example.com
# Fetches unseen messages, tries to look them up in the quarantine
# If successful, moves them to a destination folder.


# CLI opts:
# --oneshot:  Connect to all defined mailboxes, look for unread msgs, do the thing.
# --daemon:   Do "oneshot", then sleep waiting for incoming messages.
# --cf=hadwin.yaml	Load this config file, by default "hadwin.yaml"

# Config file:
# hadwin.conf: 

import sys

if sys.version_info[0] < 3:
	raise Exception("Must be running Python 3");

import yaml, argparse

ap = argparse.ArgumentParser(description='hadwin, acts on is-spam / not-spam emails.');
default_configfile="hadwin.yaml"
ap.add_argument('--cf', metavar='FILE', type=str, help="Config file for hadwin", default=default_configfile);
ap.add_argument('--oneshot', help="Process each IMAP account/message one time and exit.", action='store_true');
ap.add_argument('--daemon', help="Run as a daemon, connecting and watching each mailbox.", action='store_true');
args = ap.parse_args()

# Parse in the config file.
conf = yaml.safe_load(open(args.cf))

print(" yaml conf: " + yaml.dump(conf))

# Set up logging.
import logging
import logging.config

logging.config.dictConfig(yaml.safe_load(open('logging.yaml')))

logger = logging.getLogger(sys.argv[0])

logger.info("%s starting up." % (sys.argv[0]))

import hadwinImap
import hadwinPPSAPI

ppsapi = hadwinPPSAPI.PPSAPI(conf['ProofpointServer'])
imapServers = list(map( lambda x: hadwinImap.hadwinImap(x), conf['IMAP']))


if (args.oneshot):
	for i in imapServers:
		i.setPPSAPI(ppsapi)
		i.processOnce()

# vim: ts=4:sw=4
